<?php
/**
 * Template Name: Gym Locator
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$templates = array( 'gym-locator.twig' );

the_post();

Timber::render( $templates, $context );