# Thrive App  
**DO NOT USE OR RELY ON THE IN-BROWSER PREVIEWER!!!**  
**IT IS 100% INCORRECT IN DISPLAY AND FUNCTIONALITY.**  
**YOU WILL HAVE TO REPEATEDLY PUSH AND UPDATE TO TEST ON A REAL DEVICE.**  
  
### Applications  
1. **VS Code** or **Sublime**  
2. **Codekit 3** *if on Mac* or **Prepros 6** *if on Windows*  
3. **PHP 7.1+**  
4. **Local by Flywheel** to spin up a local server (*nginx + Varnish + PHP 7.1+*)  
	- *Preferred settings should have this by default*  
  
### Plugins  
1. **Timber**  
2. **ACF Pro** (*Advanced Custom Fields Pro*)  
3. **Gravity Forms**  
4. **WP Pusher**  
5. **WP Migrate DB Pro** (*When launching*)  
6. **AppPresser - Mobile App Framework**  
7. **AppPush - Push Notifications**  
8. **AppGeo - Geolocation Data**  
  
### AppPresser Customizer  
1. *https://myapppresser.com/*  
  
  
### Folder Structure  
1. **inc** folder - used for custom post types  
2. **static** folder - has sub folders for images, javascript, and scss  
3. **templates** folder - used for `.twig` php files  
4. **root** folder *theme root* - contains php base files, tool configs, and the main **style.css** file  
  
### Additional Notes  
- **The `_alt-styles-main.scss` partial is for slight mods to the main class(es) associated with that section/element.**  
- `_front-page.scss` **contains *almost all* of the core styles used sitewide. Other utility things will be in their respectively named files/partials.**   