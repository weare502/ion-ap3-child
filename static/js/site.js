/* global jQuery */
(function($) {
	$(document).ready(showMore); 	   // show More Button Handler
	$(document).ready(switchCategory); // category filter for Archive Page
	

	// "Show More" Button Function
    function showMore() {
        $('#load-more-posts').click(function() {

            // Toggle the box class to show it
            $('.show-more-boxes').toggleClass('stagger-boxes');

            // toggle the button's text
            var btn = document.getElementById('load-more-posts');

            if( btn.innerHTML === "See More" ) {
                btn.innerHTML = "See Less";
            } else {
                btn.innerHTML = "See More";
            }
        });
    }

    function switchCategory() {

        // button ID vars
        var allStory = $('#all-stories');
        var finStory = $('#financial-stories');
        var physStory = $('#physical-stories');
        var emoStory = $('#emotional-stories');
		var nutStory = $('#nutritional-stories');
		var wellStory = $('#wellbeing-stories');
        
        // div ID vars
        var allCat = $('#all-category');
        var finCat = $('#financial-category');
        var physCat = $('#physical-category');
        var emoCat = $('#emotional-category');
		var nutCat = $('#nutritional-category');
		var wellCat = $('#wellbeing-category');

        // All Items (if this is selected, only it can show and all others hide)
        allStory.click(function() {

            if( allCat.hasClass('main-content__posts') ) {
                allCat.addClass('hide-this');
                allCat.removeClass('main-content__posts');
                allStory.removeClass('selected-category');
            }

            else {
                allCat.removeClass('hide-this');
                allCat.addClass('main-content__posts');
                allStory.addClass('selected-category');

                // hide financial posts
                finCat.removeClass('main-content__posts');
                finCat.addClass('hide-this');
                finStory.removeClass('selected-category');
                
                // hide physical posts
                physCat.removeClass('main-content__posts');
                physCat.addClass('hide-this');
                physStory.removeClass('selected-category');
                
                // hide emotional posts
                emoCat.removeClass('main-content__posts');
                emoCat.addClass('hide-this');
                emoStory.removeClass('selected-category');

                //hide nutritional posts
                nutCat.removeClass('main-content__posts');
                nutCat.addClass('hide-this');
				nutStory.removeClass('selected-category');
				
				// hide wellbeing posts
				wellCat.removeClass('main-content__posts');
				wellCat.addClass('hide-this');
				wellStory.removeClass('selected-category');
            }
        });

        // Financial Items
        finStory.click(function() {

            if( finCat.hasClass('hide-this') ) {

                finCat.addClass('main-content__posts');
                finCat.removeClass('hide-this');
                finStory.addClass('selected-category');

                // hide all posts
                allCat.removeClass('main-content__posts');
                allCat.addClass('hide-this');
                allStory.removeClass('selected-category');
                
                // hide physical posts
                physCat.removeClass('main-content__posts');
                physCat.addClass('hide-this');
                physStory.removeClass('selected-category');
                
                // hide emotional posts
                emoCat.removeClass('main-content__posts');
                emoCat.addClass('hide-this');
                emoStory.removeClass('selected-category');

                //hide nutritional posts
                nutCat.removeClass('main-content__posts');
                nutCat.addClass('hide-this');
				nutStory.removeClass('selected-category');
				
				// hide wellbeing posts
				wellCat.removeClass('main-content__posts');
				wellCat.addClass('hide-this');
				wellStory.removeClass('selected-category');
            }
            else {
                finCat.removeClass('main-content__posts');
                finCat.addClass('hide-this');
                finStory.removeClass('selected-category');
            }
        });

        // Physical Items
        physStory.click(function() {

            if( physCat.hasClass('hide-this') ) {

                physCat.addClass('main-content__posts');
                physCat.removeClass('hide-this');
                physStory.addClass('selected-category');

                // hide all posts
                allCat.removeClass('main-content__posts');
                allCat.addClass('hide-this');
                allStory.removeClass('selected-category');

                // hide financial posts
                finCat.removeClass('main-content__posts');
                finCat.addClass('hide-this');
                finStory.removeClass('selected-category');
                
                // hide emotional posts
                emoCat.removeClass('main-content__posts');
                emoCat.addClass('hide-this');
                emoStory.removeClass('selected-category');

                //hide nutritional posts
                nutCat.removeClass('main-content__posts');
                nutCat.addClass('hide-this');
				nutStory.removeClass('selected-category');
				
				// hide wellbeing posts
				wellCat.removeClass('main-content__posts');
				wellCat.addClass('hide-this');
				wellStory.removeClass('selected-category');
            }
            else {
                physCat.removeClass('main-content__posts');
                physCat.addClass('hide-this');
                physStory.removeClass('selected-category');
            }
        });

        // Emotional Items
        emoStory.click(function() {

            if( emoCat.hasClass('hide-this') ) {
                emoCat.addClass('main-content__posts');
                emoCat.removeClass('hide-this');
                emoStory.addClass('selected-category');

                // hide all posts
                allCat.removeClass('main-content__posts');
                allCat.addClass('hide-this');
                allStory.removeClass('selected-category');

                // hide financial posts
                finCat.removeClass('main-content__posts');
                finCat.addClass('hide-this');
                finStory.removeClass('selected-category');
                
                // hide physical posts
                physCat.removeClass('main-content__posts');
                physCat.addClass('hide-this');
                physStory.removeClass('selected-category');

                //hide nutritional posts
                nutCat.removeClass('main-content__posts');
                nutCat.addClass('hide-this');
				nutStory.removeClass('selected-category');
				
				// hide wellbeing posts
				wellCat.removeClass('main-content__posts');
				wellCat.addClass('hide-this');
				wellStory.removeClass('selected-category');
            }
            else {
                emoCat.removeClass('main-content__posts');
                emoCat.addClass('hide-this');
                emoStory.removeClass('selected-category');
            }
        });

        // Nutritional Items
        nutStory.click(function() {

            if( nutCat.hasClass('hide-this') ) {
                nutCat.addClass('main-content__posts');
                nutCat.removeClass('hide-this');
                nutStory.addClass('selected-category');

                // hide all posts
                allCat.removeClass('main-content__posts');
                allCat.addClass('hide-this');
                allStory.removeClass('selected-category');

                // hide financial posts
                finCat.removeClass('main-content__posts');
                finCat.addClass('hide-this');
                finStory.removeClass('selected-category');
                
                // hide physical posts
                physCat.removeClass('main-content__posts');
                physCat.addClass('hide-this');
                physStory.removeClass('selected-category');
                
                // hide emotional posts
                emoCat.removeClass('main-content__posts');
                emoCat.addClass('hide-this');
				emoStory.removeClass('selected-category');
				
				// hide wellbeing posts
				wellCat.removeClass('main-content__posts');
				wellCat.addClass('hide-this');
				wellStory.removeClass('selected-category');
            }
            else {
                nutCat.removeClass('main-content__posts');
                nutCat.addClass('hide-this');
                nutStory.removeClass('selected-category');
            }
		});
		
		// Wellbeing Items
        wellStory.click(function() {

            if( wellCat.hasClass('hide-this') ) {
                wellCat.addClass('main-content__posts');
                wellCat.removeClass('hide-this');
                wellStory.addClass('selected-category');

                // hide all posts
                allCat.removeClass('main-content__posts');
                allCat.addClass('hide-this');
                allStory.removeClass('selected-category');

                // hide financial posts
                finCat.removeClass('main-content__posts');
                finCat.addClass('hide-this');
                finStory.removeClass('selected-category');
                
                // hide physical posts
                physCat.removeClass('main-content__posts');
                physCat.addClass('hide-this');
                physStory.removeClass('selected-category');
                
                // hide emotional posts
                emoCat.removeClass('main-content__posts');
                emoCat.addClass('hide-this');
				emoStory.removeClass('selected-category');
            }
            else {
                wellCat.removeClass('main-content__posts');
                wellCat.addClass('hide-this');
                wellStory.removeClass('selected-category');
            }
        });
    }

})(jQuery)