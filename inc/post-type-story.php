<?php

$labels = array(
	'name'               => __( 'Stories', 'thrive' ),
	'singular_name'      => __( 'Story', 'thrive' ),
	'add_new'            => _x( 'Add New Story', 'thrive', 'thrive' ),
	'add_new_item'       => __( 'Add New Story', 'thrive' ),
	'edit_item'          => __( 'Edit Story', 'thrive' ),
	'new_item'           => __( 'New Story', 'thrive' ),
	'view_item'          => __( 'View Story', 'thrive' ),
	'search_items'       => __( 'Search Stories', 'thrive' ),
	'not_found'          => __( 'No Stories found', 'thrive' ),
	'not_found_in_trash' => __( 'No Stories found in Trash', 'thrive' ),
	'parent_item_colon'  => __( 'Parent Story:', 'thrive' ),
	'menu_name'          => __( 'Stories', 'thrive' ),
);

$args = array(
	'labels'              => $labels,
	'show_in_rest'		  => true, // make this accessible to the WP-API
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array( 'category' ),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'		  => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-book-alt',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title', 'editor' ),
);

register_post_type( 'story', $args );