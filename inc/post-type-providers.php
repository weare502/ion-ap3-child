<?php

$labels = array(
	'name'               => __( 'Providers', 'thrive' ),
	'singular_name'      => __( 'Provider', 'thrive' ),
	'add_new'            => _x( 'Add New Provider', 'thrive', 'thrive' ),
	'add_new_item'       => __( 'Add New Provider', 'thrive' ),
	'edit_item'          => __( 'Edit Provider', 'thrive' ),
	'new_item'           => __( 'New Provider', 'thrive' ),
	'view_item'          => __( 'View Provider', 'thrive' ),
	'search_items'       => __( 'Search Providers', 'thrive' ),
	'not_found'          => __( 'No Providers found', 'thrive' ),
	'not_found_in_trash' => __( 'No Providers found in Trash', 'thrive' ),
	'parent_item_colon'  => __( 'Parent Provider:', 'thrive' ),
	'menu_name'          => __( 'Providers', 'thrive' ),
);

$args = array(
	'labels'              => $labels,
	'show_in_rest'		  => true, // make this accessible to the WP-API
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array( 'category' ),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-networking',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title' ),
);

register_post_type( 'provider', $args );