<?php
/**
 * Template Name: Contact
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

the_post();

$templates = array( 'contact.twig' );

Timber::render( $templates, $context );