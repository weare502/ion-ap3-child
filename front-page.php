<?php
/**
 * Template Name: Home
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
	return;
}

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// Sort posts by date in chronological order.
$context['stories'] = Timber::get_posts(array( 'post_type' => 'story', 'posts_per_page' => 6, 'orderby' => 'date' ));

// Get the categories for the post (post_type_name.category -> will return the category checked)
$context['category'] = Timber::get_term(['taxonomy' => 'category']);

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );