<?php
/**
 * Template Name: Financial
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['stories'] = Timber::get_posts(array( 'post_type' => 'story', 'posts_per_page' => -1, 'orderby' => 'date' ));

// Get the categories for the post (post_type_name.category -> will return the category checked)
$context['category'] = Timber::get_term(['taxonomy' => 'category']);

the_post();

$templates = array( 'financial-page.twig' );

Timber::render( $templates, $context );