<?php

// change Timber views directory to templates
Timber::$locations = __DIR__ . '/templates';

class ThriveApp extends TimberSite {

     function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'wp_insert_post_data', array( $this, 'mce_anchor_class_addition' ) );

		parent::__construct();
    }

    function add_to_context( $context ) {
		$context['site'] = $this;
		$context['year'] = date('Y');
		$context['is_home'] = is_home();
		$context['plugin_content'] = TimberHelper::ob_function( 'the_content' );

		// Global ACF values for the "Main Header Content" Field Group
		$context['main_header'] = get_field('large_heading');
		$context['sub_header'] = get_field('sub_heading');
		$context['btn_text'] = get_field('button_text');
		$context['btn_link'] = get_field('button_link');
		$context['sub_btn_text'] = get_field('sub_button_text');

		return $context;
	}

	function enqueue_scripts() {
        // parent ap3 ion styles
		$version = '1.9.2';

		// AP3 ION Parent Styles
		wp_enqueue_style( 'ap3-ion-style', get_template_directory_uri() . '/style.css', null, $version );

        // child ap3 ion styles
		wp_enqueue_style( 'ap3-child-style', get_stylesheet_uri(), null, $version );

		// js enqueue for archive filter and show-more button on post pages
		wp_enqueue_script( 'ap3-child-script', get_stylesheet_directory_uri() . "/static/js/site-min.js", array( 'jquery' ), '20182277' );
    }

    // register custom post types for App (show_in_rest => true   : is set for all cpts)
    function register_post_types() {
		require 'inc/post-type-story.php';
		require 'inc/post-type-providers.php';
	}

	//privacy policy, liability disclaimer, and Thrive Certification Scorecard sample
	function thrive_render_footer_menu() {
		wp_nav_menu( array(
			'theme_location' => 'footer',
			'container' => false,
			// 'menu_class' => '',
			'menu_id' => 'footer-menu',
		) );	
	}

	// adds classes to all anchor tags within post_content(); (editor box) - this allows them to open in the devices native browser instead of the built-in browser
	function mce_anchor_class_addition( $postarr ) {
		$postarr['post_content'] = str_replace('<a ', '<a class="external system">', $postarr['post_content'] );
		return $postarr;
	}
}

new ThriveApp();